# Client Demo using Hansel WebSDK 
Current domain: https://gracious-colden-fa5df9.netlify.com

## Deployment Instructions
1. Sign up on Netlify
2. Click on **New Site from Git**
3. Click on **BitBucket**
4. Choose this repo
5. Enter the build command (NA in this repo)
6. Enter the directory name where the website is built (NA in this repo)